
function save_options() {
	var user = document.getElementById("username");
	var password = document.getElementById("password");

	chrome.storage.sync.set({
		'unisa_username': user.value,
		'unisa_password': password.value
	}, function() {
		var status = document.getElementById("status");
		status.innerHTML = "Impostazioni salvate con successo.";
		setTimeout(function() {
			status.innerHTML = "";
		}, 750);
	});	
}

function restore_options() {

	chrome.storage.sync.get(['unisa_username', 'unisa_password'], function(response){

		var user = document.getElementById("username");
		var password = document.getElementById("password");

		if (response.unisa_username || response.unisa_password) {
			user.value = response.unisa_username;
			password.value = response.unisa_password;
		} else {
			user.value = '';
			password.value = '';
		}

	})
}

document.addEventListener('DOMContentLoaded', restore_options);
document.querySelector('#save').addEventListener('click', save_options);

$(document).ready(function(){

	function size() {
		if ($(window).height() < $('#wrap').outerHeight()) {
			$('#wrap').css({
				marginTop: 30,
				marginBottom: 30,
				top: 0,
				marginLeft: -Math.ceil($('#wrap').outerWidth() / 2)
			});
		} else {
			$('#wrap').css({
				top: '50%',
				left: '50%',
				marginTop: -Math.ceil($('#wrap').outerHeight() / 2),
				marginLeft: -Math.ceil($('#wrap').outerWidth() / 2)
			});
		}
	}

	size();
	$(window).resize(size);
});