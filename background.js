chrome.browserAction.onClicked.addListener(function(tab) {
	var url;

	chrome.storage.sync.get(['unisa_username', 'unisa_password'], function(response){

		if (response.unisa_username && response.unisa_username) {
			url = "https://" + response.unisa_username.substring(0, response.unisa_username.indexOf('@')) + ":" + response.unisa_password + "@esse3web.unisa.it/unisa/auth/Logon.do";
			chrome.tabs.create({ url: url });
		} else {
			chrome.tabs.create({ url: 'options.html' }, function(tab){
				alert("Devi inserire le tue credenziali UNISA per utilizzare correttamente l'estensione.");
			});
		}
	})
});