var name, password;
var namebox, passwordbox;
var response_username, response_password;

namebox = document.getElementsByName('username')[0];
passwordbox = document.getElementsByName('password')[0];
button = document.getElementsByClassName('button')[0];

chrome.storage.sync.get(['unisa_username', 'unisa_password'], function(response){
	if (response.unisa_username && response.unisa_password) {
		if (typeof namebox != undefined  && typeof password != undefined && typeof button != undefined) {
			namebox.value = response.unisa_username;
			passwordbox.value = response.unisa_password;
			button.click();
		} else {
			alert("Non sei su una pagina login standard di UNISA. Errore, contatta l'admin kamilmolendys@gmail.com ;)")
		}
	} else {
		chrome.tabs.create({ url: 'options.html' }, function(tab){
			alert("Devi inserire le tue credenziali UNISA per utilizzare correttamente l'estensione.");
		});
	}
})